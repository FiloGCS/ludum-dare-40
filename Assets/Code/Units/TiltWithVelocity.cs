﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TiltWithVelocity : MonoBehaviour {

    private Vector3 previousPosition;

    // Use this for initialization
    void Start() {
        previousPosition = this.transform.position;
    }

    // Update is called once per frame
    void Update() {
        float xDelta = this.transform.position.x - previousPosition.x;
        float zDelta = this.transform.position.z - previousPosition.z;
        Vector3 delta = new Vector3(xDelta, 0, zDelta).normalized;
        this.transform.rotation = Quaternion.Euler(new Vector3(zDelta*100, 0, -xDelta*100));
        previousPosition = this.transform.position;
    }
}