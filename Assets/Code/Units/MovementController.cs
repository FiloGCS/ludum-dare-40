﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour {

    public float JOYSTICK_THRESHOLD = 0.1f;
    public float speed = 10f;


    private float vInput = 0.0f;
    private float hInput = 0.0f;

    // Use this for initialization
    void Start () {

	}
	
	// Update is called once per frame
	void Update () {
       
        //Read Input
        if(Mathf.Abs(Input.GetAxis("Vertical")) > JOYSTICK_THRESHOLD){
            vInput = Input.GetAxis("Vertical");
        }else{ vInput = 0;}
        if (Mathf.Abs(Input.GetAxis("Horizontal")) > JOYSTICK_THRESHOLD){
            hInput = Input.GetAxis("Horizontal");
        }else{ hInput = 0; }

        //Apply Movement
        this.GetComponent<CharacterController>().SimpleMove(new Vector3(hInput, 0, vInput)*speed);
    }
}
