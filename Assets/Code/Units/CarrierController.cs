﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This component controls the behaviour of a ship load
public class CarrierController : MonoBehaviour {

    //Parameters
    public float MAX_CAPACITY = 5;
    public float OIL_CONSUMPTION = 0.1f;
    public float ACTION_DURATION = 0.5f;
    public ParticleSystem miningParticles;
    public ParticleSystem oilingParticles;
    public GameObject FOLLOWER;
    public GameObject EXPLOSION;

    //Public attributes
    public List<Resource> load;
    public Vector3 lastResourceVisitedPosition;
    //Private attributes
    private float actionStart = 0;
    private Resource recollecting = null;
    private Builder builder;

    public GameObject follower;


    //Start
    void Start() {
        load = new List<Resource>();
        builder = GameObject.Find("Builder").GetComponent<Builder>();

        if (FOLLOWER != null) {
            follower = (GameObject)Instantiate(FOLLOWER);
            follower.transform.position = this.transform.position + (Vector3.down * 10);
            follower.GetComponent<Follower>().target = this.transform;
        }

	}
	
	//Update
	void Update () {
        //If we are currently recolleting a resource
        if (recollecting!=null) {
            //If cargo is full
            if(load.Count >= MAX_CAPACITY) {
                //Stop recollecting
                recollecting = null;
                //Stop particles
                miningParticles.Stop();
                oilingParticles.Stop();

                //If cargo is not full
            } else {
                //Check if we recollected the next unit
                if (Time.time - actionStart > ACTION_DURATION) {
                    //Add the recollected unit to the cargo
                    load.Add(recollecting);
                    //Show the prop
                    GetComponent<ShowCargo>().add(recollecting.type);
                    //Restart the action
                    actionStart = Time.time;
                }
            }
        }
        
	}

    //Fixed Update
    void FixedUpdate() {
        builder.burnOil(OIL_CONSUMPTION * Time.fixedDeltaTime);
    }

    //On Trigger Enter
    void OnTriggerEnter(Collider other) {
        //When next to a resource
        if (other.gameObject.layer == LayerMask.NameToLayer("Resource")) {
            //Start recollecting
            recollecting = other.GetComponent<Resource>();
            lastResourceVisitedPosition = other.transform.position;
            actionStart = Time.time;
            //Play particles
            if (recollecting.type == "mineral"){
                miningParticles.Play();
            } else {
                oilingParticles.Play();
            }

        //When next to a deposit
        }else if (other.gameObject.layer == LayerMask.NameToLayer("Deposit")) {
            //Drop the cargo
            builder.GetComponent<Builder>().addMinerals(getResourceCount("mineral"));
            builder.GetComponent<Builder>().addOil(getResourceCount("oil"));
            //Destroy
            Destroy(this.gameObject);
        }else if(other.gameObject.layer == LayerMask.NameToLayer("Lethal") && other.gameObject != this.gameObject) {
            //Destroy
            GameObject myExplosion = (GameObject) Instantiate(EXPLOSION);
            myExplosion.transform.position = this.transform.position;
            Destroy(this.gameObject);
        }
    }



    //On Trigger Exit
    void OnTriggerExit(Collider other) {
        //If I was mining  &&  I have particle system
        if (recollecting != null) {
            miningParticles.Stop();
            oilingParticles.Stop();
        }
        recollecting = null;
    }

    public float getResourceCount(string type) {
        float result = 0.0f;
        foreach (Resource resource in load) {
            if(resource.type == type) {
                result += 1;
            }
        }
        return result;
    }
    
}
