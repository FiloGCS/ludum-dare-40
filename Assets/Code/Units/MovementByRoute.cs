﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This component leads the movement of an object based on the route manager
public class MovementByRoute : MonoBehaviour {
    
    //Public attributes
    public RouteManager routeManager;
    public int routeIndex;
    //Private attributes
    private float timer;

    //Constructor
    public MovementByRoute(RouteManager routeManager, int routeIndex) {
        this.routeManager = routeManager;
        this.routeIndex = routeIndex;
    }

	//Start
	void Start () {
        //HACK
        if(routeManager == null) {
            Destroy(this.gameObject);
        }
        timer = 0;
        this.transform.position = routeManager.getStartingPosition(routeIndex);
	}
	
	//Fixed Update
	void FixedUpdate () {
        timer += Time.fixedDeltaTime;
        if (routeManager.getLastPointTime(routeIndex) <= timer) {
            //I'm supposed to die :(
            Destroy(this.gameObject);
        }
        this.transform.position = routeManager.getPosition(routeIndex,timer);
	}
}
