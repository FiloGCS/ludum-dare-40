﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Follower : MonoBehaviour {

    public Transform target;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		if (target == null) {
            Destroy(this.gameObject);
        } else {
            //Look at the target 
            if (Vector3.Magnitude(target.position - this.transform.position) > 0.01f) {
                this.transform.rotation = Quaternion.Lerp(this.transform.rotation, Quaternion.LookRotation(target.position - this.transform.position, Vector3.up), 0.5f);
                this.transform.position = Vector3.Lerp(this.transform.position, target.position, 0.15f);
            }
        }

	}
}
