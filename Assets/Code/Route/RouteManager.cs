﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This controller stores all the routes and responds to the MovementByRoute component queries
public class RouteManager : MonoBehaviour {
    
    //Parameters
    public Cameraman CAMERAMAN;
    //Public attributes
    public List<Route> routes;
    //Private attributes
    public GameObject trackedShip;
    private int newRouteIndex;
    private float newRouteTimer;

    //Start
    void Start() {
        routes = new List<Route>();
    }

    //Fixed Update
    void FixedUpdate() {
       
        ///If tracking a new route
        if (trackedShip != null) {
            newRouteTimer += Time.fixedDeltaTime;
            //Add new routepoint
            routes[newRouteIndex].addPoint(newRouteTimer, trackedShip.transform.position, trackedShip.GetComponent<CharacterController>().velocity);
        }

    }

    //Creates a new route
    public int createRoute(GameObject ship) {
        if ( trackedShip == null) {
            print("Tracking new Route");
            //Track this GameObject
            trackedShip = ship;
            //Focus the camera on it
            CAMERAMAN.Focus(trackedShip.transform);
            //Create a new route with the according period offset
            routes.Add(new Route());
            newRouteTimer = 0.0f;
            return newRouteIndex = routes.Count - 1;
        }
        return -1;
    }

    //Finishes the currently created route
    public void finishRoute() {
        if (trackedShip != null) {
            Destroy(trackedShip);
            trackedShip = null;
            newRouteIndex = -1;
            newRouteTimer = 0.0f;
        }
    }

    //Returns the position for the given route and time
    public Vector3 getPosition(int routeIndex, float time) {
        return routes[routeIndex].getPosition(time);
    }

    //Returns the starting position for the given route
    public Vector3 getStartingPosition(int routeIndex) {
        return routes[routeIndex].getStartingPosition();
    }

    public float getLastPointTime(int routeIndex) {
        return routes[routeIndex].getLastPointTime();
    }
}
