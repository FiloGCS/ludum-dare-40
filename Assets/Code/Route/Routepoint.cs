﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Stores the info for a route point
public class Routepoint{
    
    //Public attributes
    public float time;
    public Vector3 position;
    public Vector3 velocity;

    //Constructor
    public Routepoint(float time, Vector3 position) {
        this.time = time;
        this.position = position;
    }
    public Routepoint(float time, Vector3 position, Vector3 velocity) {
        this.time = time;
        this.position = position;
        this.velocity = velocity;
    }
}
