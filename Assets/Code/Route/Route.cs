﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This class stores the information of a route
public class Route{
    
    //Private attributes
    private List<Routepoint> points;
    
    //Constructor
    public Route() {
        this.points = new List<Routepoint>();
    }

    //Adds a routepoint to this route
    public void addPoint(float time, Vector3 position) {
        points.Add(new Routepoint(time, position));
    }
    public void addPoint(float time, Vector3 position, Vector3 velocity) {
        points.Add(new Routepoint(time, position, velocity));
    }

    //Returns the position for the given time
    public Vector3 getPosition(float time) {
        //TODO Can be optimized storing the last point queried
        for(int i=0; i<points.Count; i++) {
            if(points[i].time >= time) {
                //If there is a next point to interpolate to
                Vector3 pa = Vector3.zero;
                if (i + 1 < points.Count) {
                    float ta = time - points[i].time;
                    float tb = points[i + 1].time - points[i].time;
                    Vector3 pb = points[i + 1].position - points[i].position;
                    pa = pb * ta / tb;
                }
                return points[i].position + pa;
            }
        }
        //If we have reached the end of the route...
        return Vector3.negativeInfinity;
    }

    //Returns the starting position of the route
    public Vector3 getStartingPosition() {
        return points[0].position;
    }

    //Gets the last timestamp of the route
    public float getLastPointTime() {
        return points[points.Count - 1].time;
    }
}