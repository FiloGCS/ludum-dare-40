﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This component controls the building behaviour
public class Buildable : MonoBehaviour {

    //Parameters
    public Material AVAILABLE_PLANNING_MATERIAL;
    public Material UNAVAILABLE_PLANNING_MATERIAL;
    public Material BUILT_MATERIAL;
    public float MINERAL_COST;
    //public float BURN_RATE;
    //Public attributes
    public bool available = true;
    public string name = "EMPTY DESCRIPTION";
    public string initialCost = "EMPTY DESCRIPTION";
    public string recurringCost = "EMPTY DESCRIPTION";
    public string description = "EMPTY DESCRIPTION";
    public string lore = "EMPTY DESCRIPTION";
    public bool inClearSpace = true;

    //Private attributes
    private bool inPlanningMode = false;


    public void setPlanningMode(bool isPlanningMode) {
        //Modify the visuals for planning mode
        if (isPlanningMode) {
            inPlanningMode = true;
            GetComponent<Renderer>().material = AVAILABLE_PLANNING_MATERIAL;
            //Modify the visuals for the real material
        } else {
            inPlanningMode = false;
            GetComponent<Renderer>().material = BUILT_MATERIAL;
        }
    }

    public void setPlanningAvailable(bool newAvailable) {
        //If they tell me I'm available and I wasn't
        if (newAvailable && !available) {
            available = true;
            GetComponent<Renderer>().material = AVAILABLE_PLANNING_MATERIAL;
            //If they tell me I'm not available and I was
        } else if (!newAvailable && available) {
            available = false;
            GetComponent<Renderer>().material = UNAVAILABLE_PLANNING_MATERIAL;
        }
    }

    public virtual void Spawn() {

    }

    void OnTriggerEnter(Collider other) {
        print("Trigger enter");
        if (other.gameObject.layer == LayerMask.NameToLayer("Deposit") || other.gameObject.layer == LayerMask.NameToLayer("Resource")) {
            this.inClearSpace = false;
        }
    }

    void OnCollisionEnter(Collision collision) {
        print("HEY YA");
    }

    void OnTriggerExit(Collider other) {
        if (other.gameObject.layer == LayerMask.NameToLayer("Deposit") || other.gameObject.layer == LayerMask.NameToLayer("Resource")) {
            this.inClearSpace = true;
        }
    }
}