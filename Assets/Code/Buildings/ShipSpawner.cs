﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShipSpawner : Buildable {

    //Parameters
    public GameObject SHIP_GO;
    public GameObject TRACER_SHIP_GO;
    public float PERIOD;
    public float OIL_BURNT_PER_SHIP;
    public float MINERALS_BURNT_PER_SHIP;
    //Public attributes
    public float timer = 0;
    public int routeIndex;
    //Private attributes
    private bool working = false;
    private GameObject tracerShip = null;
    private Vector3 startingTracerShipPosition;
    private RouteManager routeManager;
    private Builder builder;

    //Spawn
    public override void Spawn() {
        builder = GameObject.Find("Builder").GetComponent<Builder>();
        routeManager = GameObject.Find("RouteManager").GetComponent<RouteManager>();  //HACK Se puede guardar así?
        working = true;
        tracerShip = (GameObject)Instantiate(TRACER_SHIP_GO, this.transform.position + Vector3.up * 2, this.transform.rotation);
        startingTracerShipPosition = tracerShip.transform.position;
        GameObject.Find("Cameraman").GetComponent<Cameraman>().Focus(tracerShip.transform);
    }

    //Fixed Update
    void FixedUpdate() {
        if (working) {
            //Did the tracer start moving?
            if(tracerShip != null && tracerShip.transform.position != this.transform.position) {
                routeIndex = routeManager.createRoute(tracerShip);
                tracerShip = null;
            }

            timer += Time.fixedDeltaTime;
            //Is it time to spawn a new ship?
            if (((timer % PERIOD) - Time.fixedDeltaTime < 0) && builder.minerals >= MINERALS_BURNT_PER_SHIP) {
                SpawnShip();
            }
        }
    }

    public void SpawnShip() {
        //Burn the oil
        builder.burnOil(OIL_BURNT_PER_SHIP);
        builder.burnMinerals(MINERALS_BURNT_PER_SHIP);
        //Instantiate the ship
        GameObject newShip = Instantiate(SHIP_GO,this.transform.position,this.transform.rotation);
        //Assign the route
        newShip.GetComponent<MovementByRoute>().routeIndex = routeIndex;
        newShip.GetComponent<MovementByRoute>().routeManager = routeManager;
    }

}