﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ButtonScript : MonoBehaviour , IPointerEnterHandler {

    public int INDEX;

    public void OnPointerEnter(PointerEventData eventData) {
        GameObject.Find("UI").GetComponent<InterfaceController>().Hover(INDEX);
    }
}
