﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowCargo : MonoBehaviour {

    public GameObject PROP_MINERALS;
    public GameObject PROP_OIL;
    public float TURNING_SPEED = 3.0f;

    private List<GameObject> props;
    private float maxProps;


	//Start
	void Start () {
        props = new List<GameObject>();
        maxProps = GetComponent<CarrierController>().MAX_CAPACITY;
	}
	
	//Update
	void Update () {
        for (int i = 0; i < props.Count; i++) {
            Vector3 propPos = this.transform.position + Vector3.up; //Rise it up
            propPos += new Vector3(Mathf.Sin(((float)i/maxProps) * 2 * Mathf.PI + Time.time * TURNING_SPEED), 0, 0);
            propPos += new Vector3(0, 0, Mathf.Cos(((float)i/maxProps) * 2 * Mathf.PI + Time.time * TURNING_SPEED));
            props[i].transform.position = Vector3.Lerp(props[i].transform.position, propPos, 0.2f);
        }
	}
    
    public void add(string resourceType) {
        GameObject newProp;
        if (resourceType == "mineral") {
            newProp = (GameObject)Instantiate(PROP_MINERALS);
            newProp.GetComponent<CargoProp>().dad = this.gameObject;
            newProp.transform.position = GetComponent<CarrierController>().lastResourceVisitedPosition;
            props.Add(newProp);
        } else if(resourceType == "oil") {
            newProp = (GameObject)Instantiate(PROP_OIL);
            newProp.GetComponent<CargoProp>().dad = this.gameObject;
            newProp.transform.position = GetComponent<CarrierController>().lastResourceVisitedPosition;
            props.Add(newProp);
        }
    }
}
