﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour {

    //Parameters
    public float ZOOM_SENSIBILITY = 0.3f;
    public float LERP_SPEED = 10;

    private float zoom = 1;
    private Vector3 cameraOffset;
    private Vector3 targetPosition;

    //Start
    void Start () {
        cameraOffset = this.transform.localPosition;
        targetPosition = this.transform.localPosition;
	}
	
	//Update
	void Update () {

        //Zoom calculation
        zoom -= Input.GetAxis("Mouse ScrollWheel") * ZOOM_SENSIBILITY;
        if (zoom < 0.75f) {
            zoom = 0.75f;
        } else if (zoom > 2) {
            zoom = 2;
        }

        //Target recalculation
        targetPosition = cameraOffset * zoom;

        //Recolocation to target
        this.transform.localPosition = Vector3.Lerp(this.transform.localPosition, targetPosition, Time.deltaTime * LERP_SPEED);

    }
}
