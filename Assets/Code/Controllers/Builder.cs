﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This controller is used to spawn buildings
public class Builder : MonoBehaviour {

    //Parameters
    public float MIN_BUILDING_DISTANCE = 2;
    public Camera CAMERA;
    public float STARTING_OIL = 100;
    public float STARTING_MINERALS = 100;
    public MeshRenderer CANVAS;

    //Public attributes
    public float oil = 0;
    public float minerals = 0;

    //Private attributes
    private List<Buildable> Buildings;
    private Buildable currentBuilding = null;


	//Start
	void Start () {
        Buildings = new List<Buildable>();
        oil = STARTING_OIL;
        minerals = STARTING_MINERALS;
        CANVAS.enabled = false;
    }
	
	//Update
	void Update () {
        //On Planning Mode
        if(currentBuilding!= null) {
            //Show canvas
            CANVAS.enabled = true;

            //Update desired position
            RaycastHit hit;
            Ray ray = CAMERA.ScreenPointToRay(Input.mousePosition);
            int layers = LayerMask.GetMask("Ground", "NonGround");
            if(Physics.Raycast(ray, out hit, Mathf.Infinity, layers)) {
                //Check for close buildings
                Buildable oldBuilding = getBuildingInRange(currentBuilding.transform.position);
                //If it's clear
                if(oldBuilding == null && hit.transform.gameObject.layer != LayerMask.NameToLayer("NonGround") && currentBuilding.inClearSpace) {
                    //Tell him he is available
                    currentBuilding.setPlanningAvailable(true);
                //If it isn't clear...
                } else {
                    //Tell him he is unavailable
                    currentBuilding.setPlanningAvailable(false);
                }

                //Update the planned building position
                currentBuilding.transform.position = hit.point;
            }

            //If clicked on the ground  &&  the building can be built there
            if (Input.GetMouseButtonDown(0) && currentBuilding.available) {
                //Tell the building that he just spawned
                currentBuilding.Spawn();
                //The deposit pays for the building
                minerals -= currentBuilding.MINERAL_COST;
                //Leave planning mode
                currentBuilding.setPlanningMode(false);
                //Add this building to the list of buildings
                Buildings.Add(currentBuilding);
                //Clear the current building
                currentBuilding = null;
            }
        } else {
            CANVAS.enabled = false;
        }

	}

    public void planBuilding(GameObject building){
        //Spawn the new building
        GameObject newBuilding = (GameObject) Instantiate(building);
        newBuilding.transform.position = Vector3.zero;
        //Set the building to planning mode
        newBuilding.GetComponent<Buildable>().setPlanningMode(true);
        //Start checking for positions for the building
        currentBuilding = newBuilding.GetComponent<Buildable>();
    }

    //Checks for the first building too close to the position given, if any
    private Buildable getBuildingInRange(Vector3 newPos) {
        foreach (Buildable building in Buildings) {
            if (Mathf.Abs(Vector3.Magnitude(building.transform.position - newPos))< MIN_BUILDING_DISTANCE){
                return building;
            }
        }
        return null;
    }


    public void addOil(float newOil) {
        oil += newOil;
    }

    public void burnOil(float burnt) {
        oil -= burnt;
        if (oil < 0) {
            print("GAME OVER");
        }
    }

    public void addMinerals(float newMinerals) {
        minerals += newMinerals;
    }

    public void burnMinerals(float burnt) {
        minerals -= burnt;
    }
}
