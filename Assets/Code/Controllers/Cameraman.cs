﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//This controller tracks the camera to its focus
public class Cameraman : MonoBehaviour {

    //Parameters
    public float CAMERA_SENSIBILITY = 1.0f;
    public float CAMERA_LERP_SPEED = 10.0f;

    //Public attributes
    public Transform focus;

    //Private attributes
    private Vector3 targetPosition;
	
	// Update is called once per frame
	void Update () {

        if(focus != null){
            //If we have a target to follow
            targetPosition = focus.position;
            this.transform.position = Vector3.Lerp(this.transform.position, targetPosition, Time.deltaTime * CAMERA_LERP_SPEED);
        }else{
            //Free Cam
            this.transform.position = this.transform.position + new Vector3(Input.GetAxis("Horizontal") * CAMERA_SENSIBILITY, 0, Input.GetAxis("Vertical") * CAMERA_SENSIBILITY);
            if(this.transform.position.x > 40) {
                this.transform.position = new Vector3(40, transform.position.y, transform.position.z);
            }
            if (this.transform.position.x < -40) {
                this.transform.position = new Vector3(-40, transform.position.y, transform.position.z);
            }
            if (this.transform.position.z > 40) {
                this.transform.position = new Vector3(transform.position.x, transform.position.y, 40);
            }
            if (this.transform.position.z < -40) {
                this.transform.position = new Vector3(transform.position.x, transform.position.y, -40);
            }
        }
	}

    public void Focus(Transform newFocus) {
        this.focus = newFocus;
    }
}
