﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InterfaceController : MonoBehaviour {

    //PARAMETERS
    public Text INFO_DESCRIPTION;
    public Text INFO_NAME;
    public Text INFO_INITIAL_COST;
    public Text INFO_RECURRING_COST;
    public Text TEXT_MINERAL_COUNT;
    public Text TEXT_OIL_COUNT;

    public Text TEXT_MAX_MINERALS;
    public Text TEXT_COUNTDOWN;

    public GameObject[] BUILDING_LIST;

    public Button BUTTON_BUILD_MINER_SPAWNER;
    public Button BUTTON_BUILD_BIG_MINER_SPAWNER;
    public Button BUTTON_BUILD_FAST_MINER_SPAWNER;

    public Slider VOLUME_SLIDER;
    public Image END_PANEL;
    public GameObject TUT_PANEL;

    //Private attributes
    private Builder builder;
    private RouteManager routeManager;
    private float maxMinerals;
    private bool didWeStart = false;
    private float countdownStartTime;
    private float countdownSeconds = 300;
    //Start
    void Start() {
        builder = GameObject.Find("Builder").GetComponent<Builder>();
        routeManager = GameObject.Find("RouteManager").GetComponent<RouteManager>();
    }

    //Update
    void Update() {
        //Update Text
        TEXT_MINERAL_COUNT.text = builder.minerals.ToString("F0");
        TEXT_OIL_COUNT.text = builder.oil.ToString("F1");

        TEXT_MAX_MINERALS.text = "max minerals reached: " + maxMinerals.ToString("F2");
        if (!didWeStart && builder.minerals != builder.STARTING_MINERALS) {
            didWeStart = true;
            countdownStartTime = Time.time;
        }



        if (didWeStart) {
            float timeleft = countdownSeconds - (Time.time - countdownStartTime);
            float minutes = Mathf.Floor(timeleft / 60);
            float seconds = Mathf.Floor(timeleft % 60);
            if (timeleft <= 0) {
                minutes = 0;
                seconds = 0;
                TEXT_COUNTDOWN.color = Color.white;
            }
            if (seconds < 10) {
                TEXT_COUNTDOWN.text = minutes.ToString("F0") + ":0" + seconds.ToString("F0");
            } else {
                TEXT_COUNTDOWN.text = minutes.ToString("F0") + ":" + seconds.ToString("F0");
            }

            //Update max minerals
            if (builder.minerals > maxMinerals && timeleft > 0) {
                maxMinerals = builder.minerals;
            }
        }

        TUT_PANEL.SetActive(routeManager.trackedShip != null);

        //Update volume
        AudioListener.volume = VOLUME_SLIDER.value;
        
        //Check Buttons
        float mineralcount = builder.minerals;


        if(routeManager.trackedShip != null) {
            BUTTON_BUILD_MINER_SPAWNER.interactable = false;
            BUTTON_BUILD_BIG_MINER_SPAWNER.interactable = false;
            BUTTON_BUILD_FAST_MINER_SPAWNER.interactable = false;
        } else {
            BUTTON_BUILD_MINER_SPAWNER.interactable = true;
            BUTTON_BUILD_BIG_MINER_SPAWNER.interactable = true;
            BUTTON_BUILD_FAST_MINER_SPAWNER.interactable = true;
            if (mineralcount < 20) {
                BUTTON_BUILD_MINER_SPAWNER.interactable = false;
            }
            if (mineralcount < 30) {
                BUTTON_BUILD_FAST_MINER_SPAWNER.interactable = false;
            }
            if (mineralcount < 50) {
                BUTTON_BUILD_BIG_MINER_SPAWNER.interactable = false;
            }
        }

    }

    void FixedUpdate() {
        if(builder.oil < 0) {
            END_PANEL.raycastTarget = true;
            END_PANEL.color = Color.Lerp(END_PANEL.color, new Color(0.1f, 0.1f, 0.1f, 1.0f), 0.2f);
            TEXT_OIL_COUNT.text = "0";
        }
    }

    public void TryToBuild(int buildingIndex) {

        builder.planBuilding(BUILDING_LIST[buildingIndex]);
    }


    //When hovering a button, display the building description
    public void Hover(int index) {
        INFO_INITIAL_COST.text = "initial cost: " + BUILDING_LIST[index].GetComponent<Buildable>().initialCost;
        INFO_RECURRING_COST.text = "recurring cost: " + BUILDING_LIST[index].GetComponent<Buildable>().recurringCost;
        INFO_NAME.text = BUILDING_LIST[index].GetComponent<Buildable>().name;
        INFO_DESCRIPTION.text = BUILDING_LIST[index].GetComponent<Buildable>().description;
    }

    public void Exit() {
        print("exit");
        Application.Quit();
    }

    public void Restart() {
        print("restart");
        Application.LoadLevel(Application.loadedLevel);
    }
    
}

