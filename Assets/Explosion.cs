﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

    private float myTime;

	//Start
	void Start () {
        GetComponent<ParticleSystem>().Play();
        myTime = Time.time + 1;

	}
	
	//Update
	void Update () {
		if(Time.time > myTime) {
            Destroy(this.gameObject);
        }
	}
}
