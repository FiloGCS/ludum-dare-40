// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Selection Marker"
{
	Properties
	{
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_Color1("Color 1", Color) = (0.6176471,0.5676903,0.5676903,0)
		_Color0("Color 0", Color) = (0.8529412,0.8217993,0.570718,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Transparent"  "Queue" = "Transparent+0" "IgnoreProjector" = "True" "IsEmissive" = "true"  }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Unlit alpha:fade keepalpha noshadow exclude_path:deferred 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform float4 _Color0;
		uniform float4 _Color1;
		uniform sampler2D _TextureSample0;
		uniform float4 _TextureSample0_ST;

		inline fixed4 LightingUnlit( SurfaceOutput s, half3 lightDir, half atten )
		{
			return fixed4 ( 0, 0, 0, s.Alpha );
		}

		void surf( Input i , inout SurfaceOutput o )
		{
			float2 uv_TexCoord2 = i.uv_texcoord * float2( 1,1 ) + float2( 0,0 );
			float4 temp_cast_0 = (uv_TexCoord2.y).xxxx;
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			float4 lerpResult15 = lerp( temp_cast_0 , tex2D( _TextureSample0, uv_TextureSample0 ) , 0.1);
			float4 lerpResult25 = lerp( _Color0 , _Color1 , lerpResult15.r);
			o.Emission = lerpResult25.rgb;
			float4 temp_cast_3 = (0.25).xxxx;
			float4 temp_cast_4 = (0.785).xxxx;
			float4 clampResult18 = clamp( lerpResult15 , temp_cast_3 , temp_cast_4 );
			float4 temp_cast_5 = (0.25).xxxx;
			float4 temp_cast_6 = (0.785).xxxx;
			float4 temp_cast_7 = (0.0).xxxx;
			float4 temp_cast_8 = (1.0).xxxx;
			o.Alpha = (temp_cast_7 + (clampResult18 - temp_cast_5) * (temp_cast_8 - temp_cast_7) / (temp_cast_6 - temp_cast_5)).r;
		}

		ENDCG
	}
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13901
-1913;29;1906;1004;806.2472;384.2071;1.141689;True;True
Node;AmplifyShaderEditor.TextureCoordinatesNode;2;-914.6171,-33.33843;Float;True;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;1,1;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;12;-975.8879,284.4432;Float;True;Property;_TextureSample0;Texture Sample 0;1;0;Assets/Textures/Ground_output.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;16;-873.6769,501.3783;Float;False;Constant;_Float0;Float 0;3;0;0.1;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.LerpOp;15;-587.9066,262.5419;Float;True;3;0;COLOR;0.0,0,0,0;False;1;COLOR;0;False;2;FLOAT;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.RangedFloatNode;19;-482.7045,632.4459;Float;False;Constant;_Float1;Float 1;2;0;0.785;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;20;-489.7045,555.4459;Float;False;Constant;_Float2;Float 2;2;0;0.25;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;23;-241.7045,730.4459;Float;False;Constant;_Float3;Float 3;2;0;0;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.ClampOpNode;18;-216.7045,351.4459;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;2;COLOR;0.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.ColorNode;27;-472.7045,-27.55408;Float;False;Property;_Color1;Color 1;1;0;0.6176471,0.5676903,0.5676903,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;24;-97.70447,826.4459;Float;False;Constant;_Float4;Float 4;2;0;1;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.ColorNode;26;-471.7045,-192.5541;Float;False;Property;_Color0;Color 0;2;0;0.8529412,0.8217993,0.570718,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.LerpOp;25;-132.7045,34.44592;Float;False;3;0;COLOR;0.0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.TFHCRemapNode;22;64.29553,529.4459;Float;True;5;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;2;COLOR;1.0,0,0,0;False;3;COLOR;0.0,0,0,0;False;4;COLOR;1.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;488.9638,71.34363;Float;False;True;2;Float;ASEMaterialInspector;0;0;Unlit;Selection Marker;False;False;False;False;False;False;False;False;False;False;False;False;False;False;True;False;False;Back;0;0;False;0;0;Transparent;0.5;True;False;0;False;Transparent;Transparent;ForwardOnly;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;False;2;SrcAlpha;OneMinusSrcAlpha;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;15;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;15;0;2;2
WireConnection;15;1;12;0
WireConnection;15;2;16;0
WireConnection;18;0;15;0
WireConnection;18;1;20;0
WireConnection;18;2;19;0
WireConnection;25;0;26;0
WireConnection;25;1;27;0
WireConnection;25;2;15;0
WireConnection;22;0;18;0
WireConnection;22;1;20;0
WireConnection;22;2;19;0
WireConnection;22;3;23;0
WireConnection;22;4;24;0
WireConnection;0;2;25;0
WireConnection;0;9;22;0
ASEEND*/
//CHKSM=AFCB3A5B6BBD55DCB66A040117B11E96D1E93DC5