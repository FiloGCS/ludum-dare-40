// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/Water"
{
	Properties
	{
		_Color("Color", Color) = (0,0,0,0)
		_Smoothness("Smoothness", Float) = 1
		_TextureSample1("Texture Sample 1", 2D) = "bump" {}
		_TextureSample0("Texture Sample 0", 2D) = "bump" {}
		_Tiling("Tiling", Vector) = (0,0,0,0)
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#include "UnityShaderVariables.cginc"
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _TextureSample0;
		uniform float2 _Tiling;
		uniform sampler2D _TextureSample1;
		uniform float4 _Color;
		uniform float _Smoothness;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_TexCoord7 = i.uv_texcoord * _Tiling + float2( 0,0 );
			float2 panner6 = ( uv_TexCoord7 + _Time.y * float2( 0.002,0.002 ));
			float2 uv_TexCoord12 = i.uv_texcoord * _Tiling + float2( 0,0 );
			float2 panner15 = ( uv_TexCoord12 + ( _Time.y + 6.56 ) * float2( -0.002,-0.002 ));
			float3 lerpResult17 = lerp( UnpackNormal( tex2D( _TextureSample0, panner6 ) ) , UnpackNormal( tex2D( _TextureSample1, panner15 ) ) , 0.5);
			o.Normal = lerpResult17;
			o.Albedo = _Color.rgb;
			o.Smoothness = _Smoothness;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13901
-1913;29;1906;1004;2136.412;-16.71603;1;True;True
Node;AmplifyShaderEditor.Vector2Node;19;-1925.716,18.60678;Float;False;Property;_Tiling;Tiling;4;0;0,0;0;3;FLOAT2;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleTimeNode;14;-1578.726,711.6653;Float;False;1;0;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.Vector2Node;8;-1512.848,84.50198;Float;False;Constant;_Vector0;Vector 0;3;0;0.002,0.002;0;3;FLOAT2;FLOAT;FLOAT
Node;AmplifyShaderEditor.TextureCoordinatesNode;12;-1471.726,374.6655;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;10,10;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.Vector2Node;13;-1461.726,535.6655;Float;False;Constant;_Vector1;Vector 1;3;0;-0.002,-0.002;0;3;FLOAT2;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleTimeNode;11;-1437.848,265.5018;Float;False;1;0;FLOAT;1.0;False;1;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;20;-1311.412,779.7161;Float;False;2;2;0;FLOAT;0.0;False;1;FLOAT;6.56;False;1;FLOAT
Node;AmplifyShaderEditor.TextureCoordinatesNode;7;-1522.848,-78.72543;Float;False;0;-1;2;3;2;SAMPLER2D;;False;0;FLOAT2;10,10;False;1;FLOAT2;0,0;False;5;FLOAT2;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.PannerNode;15;-1188.726,487.6655;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1.0;False;1;FLOAT2
Node;AmplifyShaderEditor.PannerNode;6;-1239.848,36.50197;Float;False;3;0;FLOAT2;0,0;False;2;FLOAT2;0,0;False;1;FLOAT;1.0;False;1;FLOAT2
Node;AmplifyShaderEditor.SamplerNode;5;-1005.961,13.50196;Float;True;Property;_TextureSample0;Texture Sample 0;3;0;Assets/Textures/Ground_Normal.png;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;16;-953.7254,464.6654;Float;True;Property;_TextureSample1;Texture Sample 1;3;0;Assets/Textures/Ground_Normal.png;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;18;-463.4733,485.4931;Float;False;Constant;_Float0;Float 0;4;0;0.5;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.ColorNode;1;-393,-143;Float;False;Property;_Color;Color;0;0;0,0,0,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.LerpOp;17;-444.35,265.0123;Float;False;3;0;FLOAT3;0.0;False;1;FLOAT3;0.0,0,0,0;False;2;FLOAT;0.0,0,0,0;False;1;FLOAT3
Node;AmplifyShaderEditor.RangedFloatNode;4;-225.5004,312.6119;Float;False;Property;_Smoothness;Smoothness;2;0;1;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;0,0;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Custom/Water;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;12;0;19;0
WireConnection;20;0;14;0
WireConnection;7;0;19;0
WireConnection;15;0;12;0
WireConnection;15;2;13;0
WireConnection;15;1;20;0
WireConnection;6;0;7;0
WireConnection;6;2;8;0
WireConnection;6;1;11;0
WireConnection;5;1;6;0
WireConnection;16;1;15;0
WireConnection;17;0;5;0
WireConnection;17;1;16;0
WireConnection;17;2;18;0
WireConnection;0;0;1;0
WireConnection;0;1;17;0
WireConnection;0;4;4;0
ASEEND*/
//CHKSM=8B9F9DFCB07E74289B3276F198065B227BB67812