// Made with Amplify Shader Editor
// Available at the Unity Asset Store - http://u3d.as/y3X 
Shader "Custom/SH_Ground"
{
	Properties
	{
		_TextureSample0("Texture Sample 0", 2D) = "white" {}
		_Color1("Color 1", Color) = (0.4704092,0.5882353,0.2465398,0)
		_Color0("Color 0", Color) = (0.7279412,0.6840875,0.4389057,0)
		_TextureSample1("Texture Sample 1", 2D) = "white" {}
		_TextureSample2("Texture Sample 2", 2D) = "bump" {}
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1
	}

	SubShader
	{
		Tags{ "RenderType" = "Opaque"  "Queue" = "Geometry+0" }
		Cull Back
		CGPROGRAM
		#pragma target 3.0
		#pragma surface surf Standard keepalpha addshadow fullforwardshadows 
		struct Input
		{
			float2 uv_texcoord;
		};

		uniform sampler2D _TextureSample2;
		uniform float4 _TextureSample2_ST;
		uniform float4 _Color0;
		uniform float4 _Color1;
		uniform sampler2D _TextureSample0;
		uniform float4 _TextureSample0_ST;
		uniform sampler2D _TextureSample1;
		uniform float4 _TextureSample1_ST;

		void surf( Input i , inout SurfaceOutputStandard o )
		{
			float2 uv_TextureSample2 = i.uv_texcoord * _TextureSample2_ST.xy + _TextureSample2_ST.zw;
			o.Normal = UnpackNormal( tex2D( _TextureSample2, uv_TextureSample2 ) );
			float2 uv_TextureSample0 = i.uv_texcoord * _TextureSample0_ST.xy + _TextureSample0_ST.zw;
			float2 uv_TextureSample1 = i.uv_texcoord * _TextureSample1_ST.xy + _TextureSample1_ST.zw;
			float4 tex2DNode5 = tex2D( _TextureSample1, uv_TextureSample1 );
			float4 temp_output_9_0 = ( tex2D( _TextureSample0, uv_TextureSample0 ) + tex2DNode5 );
			float4 lerpResult2 = lerp( _Color0 , _Color1 , temp_output_9_0.r);
			float4 lerpResult10 = lerp( float4(1,0.866937,0.7647059,0) , float4(0.8676471,0.9014199,1,0) , tex2DNode5.r);
			o.Albedo = ( lerpResult2 * lerpResult10 ).rgb;
			float4 temp_cast_3 = (0.0).xxxx;
			float4 temp_cast_4 = (1.0).xxxx;
			float4 temp_cast_5 = (0.1).xxxx;
			float4 temp_cast_6 = (0.5).xxxx;
			o.Smoothness = (temp_cast_5 + (temp_output_9_0 - temp_cast_3) * (temp_cast_6 - temp_cast_5) / (temp_cast_4 - temp_cast_3)).r;
			o.Alpha = 1;
		}

		ENDCG
	}
	Fallback "Diffuse"
	CustomEditor "ASEMaterialInspector"
}
/*ASEBEGIN
Version=13901
-1913;29;1906;1004;622.3298;84.9375;1;True;True
Node;AmplifyShaderEditor.SamplerNode;5;-885.0333,342.3531;Float;True;Property;_TextureSample1;Texture Sample 1;3;0;Assets/Textures/Ground_Ground_Detail.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SamplerNode;1;-854.0333,-22.64685;Float;True;Property;_TextureSample0;Texture Sample 0;0;0;Assets/Textures/Ground_output.png;True;0;False;white;Auto;False;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;3;-607.0333,-376.6469;Float;False;Property;_Color0;Color 0;1;0;0.7279412,0.6840875,0.4389057,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleAddOpNode;9;-522.1284,91.71722;Float;True;2;2;0;COLOR;0.0,0,0,0;False;1;COLOR;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.ColorNode;4;-608.0333,-211.6469;Float;False;Property;_Color1;Color 1;1;0;0.4704092,0.5882353,0.2465398,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;12;-574.9298,741.3024;Float;False;Constant;_Color3;Color 3;4;0;0.8676471,0.9014199,1,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.ColorNode;11;-569.9298,566.3024;Float;False;Constant;_Color2;Color 2;4;0;1,0.866937,0.7647059,0;0;5;COLOR;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;17;-35.93005,461.3023;Float;False;Constant;_Float1;Float 1;5;0;1;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;16;-37.93005,389.3023;Float;False;Constant;_Float0;Float 0;5;0;0;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;19;-23.93005,610.3023;Float;False;Constant;_Float3;Float 3;5;0;0.5;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.RangedFloatNode;18;-31.93005,538.3023;Float;False;Constant;_Float2;Float 2;5;0;0.1;0;0;0;1;FLOAT
Node;AmplifyShaderEditor.LerpOp;10;-305.6539,469.4687;Float;True;3;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;FLOAT;0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.LerpOp;2;-280.0333,0.3531494;Float;False;3;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;2;FLOAT;0.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.TFHCRemapNode;15;168.0699,378.3023;Float;False;5;0;COLOR;0,0,0,0;False;1;COLOR;0,0,0,0;False;2;COLOR;1,0,0,0;False;3;COLOR;0,0,0,0;False;4;COLOR;1,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.SamplerNode;14;-102.9301,769.3023;Float;True;Property;_TextureSample2;Texture Sample 2;4;0;Assets/Textures/Ground_Normal.png;True;0;True;bump;Auto;True;Object;-1;Auto;Texture2D;6;0;SAMPLER2D;;False;1;FLOAT2;0,0;False;2;FLOAT;0.0;False;3;FLOAT2;0,0;False;4;FLOAT2;0,0;False;5;FLOAT;1.0;False;5;FLOAT3;FLOAT;FLOAT;FLOAT;FLOAT
Node;AmplifyShaderEditor.SimpleMultiplyOpNode;13;-86.92981,136.3023;Float;False;2;2;0;COLOR;0,0,0,0;False;1;COLOR;0.0,0,0,0;False;1;COLOR
Node;AmplifyShaderEditor.StandardSurfaceOutputNode;0;480,297;Float;False;True;2;Float;ASEMaterialInspector;0;0;Standard;Custom/SH_Ground;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;False;Back;0;0;False;0;0;Opaque;0.5;True;True;0;False;Opaque;Geometry;All;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;True;False;0;255;255;0;0;0;0;0;0;0;0;False;2;15;10;25;False;0.5;True;0;Zero;Zero;0;Zero;Zero;OFF;OFF;0;False;0;0,0,0,0;VertexOffset;False;Cylindrical;False;Relative;0;;-1;-1;-1;-1;0;0;0;False;16;0;FLOAT3;0,0,0;False;1;FLOAT3;0,0,0;False;2;FLOAT3;0,0,0;False;3;FLOAT;0.0;False;4;FLOAT;0.0;False;5;FLOAT;0.0;False;6;FLOAT3;0,0,0;False;7;FLOAT3;0,0,0;False;8;FLOAT;0.0;False;9;FLOAT;0.0;False;10;FLOAT;0.0;False;13;FLOAT3;0,0,0;False;11;FLOAT3;0,0,0;False;12;FLOAT3;0,0,0;False;14;FLOAT4;0,0,0,0;False;15;FLOAT3;0,0,0;False;0
WireConnection;9;0;1;0
WireConnection;9;1;5;0
WireConnection;10;0;11;0
WireConnection;10;1;12;0
WireConnection;10;2;5;0
WireConnection;2;0;3;0
WireConnection;2;1;4;0
WireConnection;2;2;9;0
WireConnection;15;0;9;0
WireConnection;15;1;16;0
WireConnection;15;2;17;0
WireConnection;15;3;18;0
WireConnection;15;4;19;0
WireConnection;13;0;2;0
WireConnection;13;1;10;0
WireConnection;0;0;13;0
WireConnection;0;1;14;0
WireConnection;0;4;15;0
ASEEND*/
//CHKSM=2C003C262EBA749037D870E01027234D94468662